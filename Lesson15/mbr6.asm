org 0x7c00
mov ax,0xb800
mov es,ax

;在屏幕第2行显示字符串“GrapeOS"
mov ah,0x07 ;ah中的值一直保持不变
mov al,'G'
mov [es:160],ax
mov al,'r'
mov [es:162],ax ;每个字符对应显存中的2个字节，依次递增2个字节。
mov al,'a'
mov [es:164],ax
mov al,'p'
mov [es:166],ax
mov al,'e'
mov [es:168],ax
mov al,'O'
mov [es:170],ax
mov al,'S'
mov [es:172],ax

stop:
hlt
jmp stop 

times 510-($-$$) db 0
db 0x55,0xaa