;定义常量（作用和C语言中的#define一样）
VIDEO_CHAR_MAX_COUNT equ 2000 ;默认屏幕最多显示字符数。

org 0x7c00

;初始化段寄存器
mov ax,0xb800
mov es,ax ;本程序中es专用于指向显存段

;清屏
call func_clear_screen

stop:
hlt
jmp stop 

;清屏函数（将屏幕写满空格就实现了清屏）
;输入参数：无。
;输出参数：无。
func_clear_screen:
mov ah,0x00 ;黑底黑字
mov al,' '  ;空格
mov cx,VIDEO_CHAR_MAX_COUNT
.start_blank:
mov bx,cx ;bx=(cx-1)*2 字符对应的显存地址（从屏幕右下角往前清屏）
dec bx
shl bx,1
mov [es:bx],ax
loop .start_blank
ret

times 510-($-$$) db 0
db 0x55,0xaa